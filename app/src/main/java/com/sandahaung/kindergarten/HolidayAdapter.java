package com.sandahaung.kindergarten;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.*;

/**
 * Created by Sandah Aung on 3/9/2016.
 */

public class HolidayAdapter extends ArrayAdapter<CalendarCollection> {

    Context context;
    List<CalendarCollection> days;
    List<CalendarCollection> publicHolidays;
    List<CalendarCollection> centreEventDays;
    List<CalendarCollection> centreCloseDays;


    public HolidayAdapter(Context context,
                          List<CalendarCollection> days,
                          List<CalendarCollection> publicHolidays,
                          List<CalendarCollection> centreEventDays,
                          List<CalendarCollection> centreCloseDays) {
        super(context, android.R.layout.simple_list_item_1, days);
        this.context = context;
        this.publicHolidays = publicHolidays;
        this.centreEventDays = centreEventDays;
        this.centreCloseDays = centreCloseDays;
        this.days = days;
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public CalendarCollection getItem(int position) {
        return days.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout linearLayout = (LinearLayout) convertView;

        if (linearLayout == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            linearLayout = (LinearLayout) inflater.inflate(R.layout.holiday_list_item, null);
            TextView view = (TextView) linearLayout.findViewById(R.id.holiday_list_item_date);
            View bullet = linearLayout.findViewById(R.id.holiday_list_item_bullet);

            CalendarCollection day = days.get(position);

            if (day != null) {
                view.setText(day.date + " (" + day.event_message + ")");
            }

            for (CalendarCollection cc : publicHolidays) {
                if (day.date.trim().equals(cc.date.trim())) {
                    bullet.setBackgroundColor(context.getResources().getColor(R.color.public_holiday_orange));
                }
            }

            for (CalendarCollection cc : centreEventDays) {
                if (day.date.trim().equals(cc.date.trim())) {
                    bullet.setBackgroundColor(context.getResources().getColor(R.color.centre_event_day_blue));
                }
            }

            for (CalendarCollection cc : centreCloseDays) {
                if (day.date.trim().equals(cc.date.trim())) {
                    bullet.setBackgroundColor(context.getResources().getColor(R.color.centre_close_day_green));
                }
            }
        }

        return linearLayout;
    }
}
