package com.sandahaung.kindergarten;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Sandah Aung on 3/4/2016.
 */

public class CalendarAdapter extends BaseAdapter {
    private Context context;

    private java.util.Calendar month;
    public GregorianCalendar pmonth;
    /**
     * calendar instance for previous month for getting complete view
     */
    public GregorianCalendar pmonthmaxset;
    private GregorianCalendar selectedDate;
    int firstDay;
    int maxWeeknumber;
    int maxP;
    int calMaxP;
    int lastWeekDay;
    int leftDays;
    int mnthlength;
    String itemvalue, curentDateString;
    DateFormat df;

    private ArrayList<String> items;
    public static List<String> day_string;
    private View previousView;
    public ArrayList<CalendarCollection> publicHolidays;
    public ArrayList<CalendarCollection> centreEventDays;
    public ArrayList<CalendarCollection> centreCloseDays;

    public CalendarAdapter(Context context,
                           GregorianCalendar monthCalendar,
                           ArrayList<CalendarCollection> publicHolidays,
                           ArrayList<CalendarCollection> centreEventDays,
                           ArrayList<CalendarCollection> centreCloseDays) {

        this.publicHolidays = publicHolidays;
        this.centreEventDays = centreEventDays;
        this.centreCloseDays = centreCloseDays;

        CalendarAdapter.day_string = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        this.context = context;
        month.set(GregorianCalendar.DAY_OF_MONTH, 1);

        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());
        refreshDays();

    }

    public void setItems(ArrayList<String> items) {
        for (int i = 0; i != items.size(); i++) {
            if (items.get(i).length() == 1) {
                items.set(i, "0" + items.get(i));
            }
        }
        this.items = items;
    }

    public int getCount() {
        return day_string.size();
    }

    public Object getItem(int position) {
        return day_string.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new view for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView dayView;
        if (convertView == null) { // if it's not recycled, initialize some
            // attributes
            LayoutInflater vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.cal_item, null);

        }

        dayView = (TextView) v.findViewById(R.id.date);
        String[] separatedTime = day_string.get(position).split("-");

        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            renderPrevAndNextMonthDates(separatedTime, dayView, position);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
            renderPrevAndNextMonthDates(separatedTime, dayView, position);
        } else {
            // setting curent month's days in blue color.
            dayView.setTextColor(Color.WHITE);
            java.util.Calendar calendar = getCalendar(separatedTime[0], separatedTime[1], separatedTime[2]);

            int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
            if (dayOfWeek == java.util.Calendar.SUNDAY || dayOfWeek == java.util.Calendar.SATURDAY) {
                dayView.setTextColor(context.getResources().getColor(R.color.holiday_red));
            }

            for (CalendarCollection cc : publicHolidays) {
                if (day_string.get(position).trim().equals(cc.date.trim())) {
                    dayView.setTextColor(context.getResources().getColor(R.color.public_holiday_orange));
                }
            }

            for (CalendarCollection cc : centreEventDays) {
                if (day_string.get(position).trim().equals(cc.date.trim())) {
                    dayView.setTextColor(context.getResources().getColor(R.color.centre_close_day_green));
                }
            }

            for (CalendarCollection cc : centreCloseDays) {
                if (day_string.get(position).trim().equals(cc.date.trim())) {
                    dayView.setTextColor(context.getResources().getColor(R.color.centre_event_day_blue));
                }
            }
        }


        if (day_string.get(position).equals(curentDateString)) {

            v.setBackgroundResource(R.drawable.rounded_calender_item_today);
        } else {
            v.setBackgroundColor(Color.parseColor("#343434"));
        }


        dayView.setText(gridvalue);

        // create date string for comparison
        String date = day_string.get(position);

        if (date.length() == 1) {
            date = "0" + date;
        }
        String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }

        // show icon if date is not empty and it exists in the items array
        /*ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
        if (date.length() > 0 && items != null && items.contains(date)) {
            iw.setVisibility(View.VISIBLE);
        } else {
            iw.setVisibility(View.GONE);
        }
        */

        setEventView(v, position, dayView);

        return v;
    }

    private void renderPrevAndNextMonthDates(String[] separatedTime, TextView dayView, int position) {
        dayView.setTextColor(Color.GRAY);
        dayView.setClickable(false);
        dayView.setFocusable(false);

        java.util.Calendar calendar = getCalendar(separatedTime[0], separatedTime[1], separatedTime[2]);

        int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
        if (dayOfWeek == java.util.Calendar.SUNDAY || dayOfWeek == java.util.Calendar.SATURDAY) {
            dayView.setTextColor(context.getResources().getColor(R.color.holiday_red_grey));
        }

        for (CalendarCollection cc : publicHolidays) {
            day_string.get(position).trim();
            if (day_string.get(position).trim().equals(cc.date.trim())) {
                dayView.setTextColor(context.getResources().getColor(R.color.public_holiday_orange_grey));
            }
        }

        for (CalendarCollection cc : centreEventDays) {
            if (day_string.get(position).trim().equals(cc.date.trim())) {
                dayView.setTextColor(context.getResources().getColor(R.color.centre_close_day_green_grey));
            }
        }

        for (CalendarCollection cc : centreCloseDays) {
            if (day_string.get(position).trim().equals(cc.date.trim())) {
                dayView.setTextColor(context.getResources().getColor(R.color.centre_event_day_blue_grey));
            }
        }
    }

    private java.util.Calendar getCalendar(int year, int month, int date) {
        java.util.Calendar tempCalendar = java.util.Calendar.getInstance();
        tempCalendar.set(year, month - 1, date);
        return tempCalendar;
    }

    private java.util.Calendar getCalendar(String year, String month, String date) {
        int iYear = Integer.parseInt(year);
        int iMonth = Integer.parseInt(month);
        int iDate = Integer.parseInt(date);
        java.util.Calendar calendar = getCalendar(iYear, iMonth, iDate);
        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);
        calendar.set(java.util.Calendar.MINUTE, 0);
        calendar.set(java.util.Calendar.SECOND, 0);
        calendar.set(java.util.Calendar.MILLISECOND, 0);

        return calendar;
    }

    public View setSelected(View view, int pos) {
        if (previousView != null) {
            previousView.setBackgroundColor(Color.parseColor("#343434"));
        }

        view.setBackgroundColor(context.getResources().getColor(R.color.primary));

        int len = day_string.size();
        if (len > pos) {
            if (day_string.get(pos).equals(curentDateString)) {

            } else {

                previousView = view;

            }
        }
        return view;
    }

    public void refreshDays() {
        // clear items
        items.clear();
        day_string.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);
            day_string.add(itemvalue);

        }
    }

    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            pmonth.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }


    public void setEventView(View v, int pos, TextView txt) {

        int len = CalendarCollection.centreEventDays.size();
        for (int i = 0; i < len; i++) {
            CalendarCollection cal_obj = CalendarCollection.centreEventDays.get(i);
            String date = cal_obj.date;
            int len1 = day_string.size();
            if (len1 > pos) {

                if (day_string.get(pos).equals(date)) {
                    v.setBackgroundColor(Color.parseColor("#343434"));
                    v.setBackgroundResource(R.drawable.rounded_calender_item);
                }
            }
        }

    }


    public void getPositionList(String date, final Activity act) {

        int len = CalendarCollection.publicHolidays.size();
        for (int i = 0; i < len; i++) {
            CalendarCollection cal_collection = CalendarCollection.publicHolidays.get(i);
            String event_date = cal_collection.date;

            String event_message = cal_collection.event_message;

        }


    }

}