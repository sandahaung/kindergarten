package com.sandahaung.kindergarten;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.MaterialMenuView;

import java.util.*;

public class CalendarFragment extends Fragment implements DrawerLayoutInterface {


    public GregorianCalendar calMonth, calMonthCopy;
    private CalendarAdapter calAdapter;
    private TextView tvMonth;
    private ListView centreDayListView;
    private DrawerLayout drawerLayout;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Calendar.
     */
    // TODO: Rename and change types and number of parameters
    public static CalendarFragment newInstance(String param1, String param2) {
        CalendarFragment fragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        CalendarCollection.publicHolidays = new ArrayList<CalendarCollection>();
        CalendarCollection.publicHolidays.add(new CalendarCollection("2016-03-02", "Peasant's Day"));
        CalendarCollection.publicHolidays.add(new CalendarCollection("2016-03-27", "Armed Forces Day"));

        CalendarCollection.centreEventDays = new ArrayList<CalendarCollection>();
        CalendarCollection.centreEventDays.add(new CalendarCollection("2016-03-09", "Children Play"));
        CalendarCollection.centreEventDays.add(new CalendarCollection("2016-03-23", "Parents Meet Teachers"));

        CalendarCollection.centreCloseDays = new ArrayList<CalendarCollection>();
        CalendarCollection.centreCloseDays.add(new CalendarCollection("2016-03-05", "Centre Repairs"));
        CalendarCollection.centreCloseDays.add(new CalendarCollection("2016-03-20", "Cleaning"));

        calMonth = (GregorianCalendar) GregorianCalendar.getInstance();
        calMonthCopy = (GregorianCalendar) calMonth.clone();
        calAdapter = new CalendarAdapter(getActivity(), calMonth, CalendarCollection.publicHolidays, CalendarCollection.centreEventDays, CalendarCollection.centreCloseDays);

        tvMonth = (TextView) view.findViewById(R.id.tv_month);
        tvMonth.setText(android.text.format.DateFormat.format("MMMM yyyy", calMonth));

        ImageButton previous = (ImageButton) view.findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = (ImageButton) view.findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();
            }
        });

        GridView gridview = (GridView) view.findViewById(R.id.gv_calendar);
        gridview.setAdapter(calAdapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);
                String selectedGridDate = CalendarAdapter.day_string
                        .get(position);

                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                int gridvalue = Integer.parseInt(gridvalueString);

                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);

                ((CalendarAdapter) parent.getAdapter()).getPositionList(selectedGridDate, getActivity());
            }

        });

        List<CalendarCollection> days = union(CalendarCollection.publicHolidays, CalendarCollection.centreEventDays, CalendarCollection.centreCloseDays);
        centreDayListView = (ListView) view.findViewById(R.id.centre_days);
        HolidayAdapter adapter = new HolidayAdapter(getActivity(), days, CalendarCollection.publicHolidays, CalendarCollection.centreEventDays, CalendarCollection.centreCloseDays);
        centreDayListView.setAdapter(adapter);

        return view;
    }

    protected void setNextMonth() {
        if (calMonth.get(GregorianCalendar.MONTH) == calMonth
                .getActualMaximum(GregorianCalendar.MONTH)) {
            calMonth.set((calMonth.get(GregorianCalendar.YEAR) + 1),
                    calMonth.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            calMonth.set(GregorianCalendar.MONTH,
                    calMonth.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (calMonth.get(GregorianCalendar.MONTH) == calMonth
                .getActualMinimum(GregorianCalendar.MONTH)) {
            calMonth.set((calMonth.get(GregorianCalendar.YEAR) - 1),
                    calMonth.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            calMonth.set(GregorianCalendar.MONTH,
                    calMonth.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        calAdapter.refreshDays();
        calAdapter.notifyDataSetChanged();
        tvMonth.setText(android.text.format.DateFormat.format("MMMM yyyy", calMonth));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public <T extends Comparable<T>> List<T> union(List<T> list1, List<T> list2, List<T> list3) {
        Set<T> set = new HashSet<T>();

        set.addAll(list1);
        set.addAll(list2);
        set.addAll(list3);

        List<T> list = new ArrayList<T>(set);
        Collections.sort(list);

        return list;
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }
}
