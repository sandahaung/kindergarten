package com.sandahaung.kindergarten;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;


public class AnnouncementListFragment extends Fragment {

    HomePage homePage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.announcements, container, false);

        homePage = (HomePage) getActivity();

        return view;
    }


}
