package com.sandahaung.kindergarten;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {
    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;

    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon;

        private DrawerItemHolder() {
        }
    }

    public CustomDrawerAdapter(Context context, List<DrawerItem> listItems) {
        super(context, R.layout.custom_drawer_item, listItems);
        this.context = context;
        this.drawerItemList = listItems;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder drawerHolder;
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(R.layout.custom_drawer_item, parent, false);
            drawerHolder.ItemName = (TextView) view.findViewById(R.id.drawer_itemName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }
        DrawerItem dItem = (DrawerItem) this.drawerItemList.get(position);

        drawerHolder.ItemName.setText(dItem.getItemName());
        drawerHolder.icon.setImageResource(dItem.getImgResId());
        return view;
    }
}


