package com.sandahaung.kindergarten;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import com.balysv.materialmenu.MaterialMenu;
import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.MaterialMenuView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;


@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class HomePage extends Activity implements View.OnClickListener {

    CustomDrawerAdapter adapter;
    List<DrawerItem> dataList;


    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerContainer;
    private ListView mDrawerListView;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private Fragment fragment;

    private MaterialMenuView materialMenu;
    private int actionBarMenuState;

    private MaterialMenuView materialMenuView;
    private MaterialMenu materialIcon;
    private boolean direction;

    private boolean isDrawerOpen;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        initCustomActionBar();

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragment = new AnnouncementListFragment();
            setTitle("Announcements");
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Announcement").commit();
        }

        View parent = getWindow().getDecorView();

        materialMenuView = (MaterialMenuView) parent.findViewById(R.id.material_menu_button);
        materialMenuView.setOnClickListener(this);
        materialIcon = materialMenu;

        this.dataList = new ArrayList();
        CharSequence title = getTitle();
        this.mDrawerTitle = title;
        this.mTitle = title;
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.mDrawerContainer = (LinearLayout) findViewById(R.id.drawer_container);
        this.mDrawerListView = (ListView) findViewById(R.id.left_drawer);
        this.mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        this.dataList.add(new DrawerItem(R.drawable.announce, "Announcements"));
        this.dataList.add(new DrawerItem(R.drawable.calendar, "Calender"));
        this.dataList.add(new DrawerItem(R.drawable.attendance, "My Attendance & Temperature"));
        this.dataList.add(new DrawerItem(R.drawable.home, "Home School Activities"));
        this.dataList.add(new DrawerItem(R.drawable.teacher, "My Class"));
        this.dataList.add(new DrawerItem(R.drawable.resources, "Parent Resources"));
        this.dataList.add(new DrawerItem(R.drawable.communicate, "Communication"));
        this.dataList.add(new DrawerItem(R.drawable.password, "Change Password"));
        this.dataList.add(new DrawerItem(R.drawable.logout, "Logout"));

        this.adapter = new CustomDrawerAdapter(this, this.dataList);
        this.mDrawerListView.setAdapter(this.adapter);
        this.mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());

        this.mDrawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                materialIcon.setTransformationOffset(
                        MaterialMenuDrawable.AnimationState.BURGER_ARROW,
                        direction ? 2 - slideOffset : slideOffset
                );
            }

            @Override
            public void onDrawerOpened(android.view.View drawerView) {
                direction = true;
                isDrawerOpen = true;
            }

            @Override
            public void onDrawerClosed(android.view.View drawerView) {
                direction = false;
                isDrawerOpen = false;
            }
        });

    }

    private void initCustomActionBar() {
        ActionBar actionBar = getActionBar();
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.abs_layout);
        materialMenu = (MaterialMenuView) actionBar.getCustomView().findViewById(R.id.material_menu_button);
        materialMenu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.material_menu_button) {
            if (isDrawerOpen)
                closeDrawer();
            else
                openDrawer();
            materialMenu.animatePressedState(intToState(actionBarMenuState));
            return;
        }
    }

    public void SelectedItem(int position) {
        // TODO Auto-generated method stub

        FragmentManager fragmentManager = getFragmentManager();
        switch (position) {
            case 0:
                fragment = new AnnouncementListFragment();

                setTitle("Announcements");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Announcement").commit();
                break;
            case 1:
                fragment = new AnnouncementListFragment();
                setTitle("Announcements");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Announcement").commit();
                break;
            case 2:
                fragment = new CalendarFragment();
                DrawerLayoutInterface drawerLayoutInterface = (DrawerLayoutInterface) fragment;
                drawerLayoutInterface.setDrawerLayout(mDrawerLayout);
                setTitle("Kindergarten  Calendar");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Calender").commit();
                break;
            case 3:
                fragment = new Fragment();
                setTitle("My Attendance & Temperature");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("MyAttendanceTemperature").commit();
                break;
            case 4:
                fragment = new Fragment();
                setTitle("Home School Activities");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("HomeSchoolActivities").commit();
                break;
            case 5:
                fragment = new Fragment();
                setTitle("My Class");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("MyClass").commit();
                break;
            case 6:
                fragment = new Fragment();
                setTitle("Parent Resources");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("ParentResources").commit();
                break;
            case 7:
                fragment = new Fragment();
                setTitle("Communication");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Communication").commit();
                break;
            case 8:
                fragment = new ChangePassword();
                setTitle("Change Password");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("ChangePassword").commit();
                break;
            case 9:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
            default:
                fragment = new AnnouncementListFragment();
                setTitle("Announcements");
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack("Announcement").commit();
                break;
        }
        this.mDrawerListView.setItemChecked(position, true);
        this.mDrawerLayout.closeDrawer(this.mDrawerContainer);

    }

    public void setTitle(CharSequence title) {
        this.mTitle = title;
        ((TextView) getActionBar().getCustomView().findViewById(R.id.title)).setText(title);
    }

    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.direction = mDrawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                long arg3) {
            // TODO Auto-generated method stub
            HomePage.this.SelectedItem(position + 1);

        }
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mDrawerContainer);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mDrawerContainer);
    }

    public static MaterialMenuDrawable.IconState intToState(int state) {
        switch (state) {
            case 0:
                return MaterialMenuDrawable.IconState.BURGER;
            case 1:
                return MaterialMenuDrawable.IconState.ARROW;
            case 2:
                return MaterialMenuDrawable.IconState.X;
            case 3:
                return MaterialMenuDrawable.IconState.CHECK;
        }
        throw new IllegalArgumentException("Must be a number [0,3)");
    }
}