package com.sandahaung.kindergarten;


public class DrawerItem {
    String ItemName;
    int imgResId;


    public DrawerItem(int imgResId, String itemName) {
        this.ItemName = itemName;
        this.imgResId = imgResId;
    }

    public String getItemName() {
        return this.ItemName;
    }

    public void setItemName(String itemName) {
        this.ItemName = itemName;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }
}
