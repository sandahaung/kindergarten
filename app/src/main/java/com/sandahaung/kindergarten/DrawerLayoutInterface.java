package com.sandahaung.kindergarten;

import android.support.v4.widget.DrawerLayout;

/**
 * Created by Sandah Aung on 3/14/2016.
 */
public interface DrawerLayoutInterface {
    public DrawerLayout getDrawerLayout();

    public void setDrawerLayout(DrawerLayout drawerLayout);
}
