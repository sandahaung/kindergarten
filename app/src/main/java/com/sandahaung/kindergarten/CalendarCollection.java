package com.sandahaung.kindergarten;

import java.util.ArrayList;

public class CalendarCollection implements Comparable<CalendarCollection> {
    public String date = "";
    public String event_message = "";

    public static ArrayList<CalendarCollection> publicHolidays;
    public static ArrayList<CalendarCollection> centreEventDays;
    public static ArrayList<CalendarCollection> centreCloseDays;

    public CalendarCollection(String date, String event_message) {

        this.date = date;
        this.event_message = event_message;

    }


    @Override
    public int compareTo(CalendarCollection another) {
        return this.date.compareTo(another.date);
    }
}
